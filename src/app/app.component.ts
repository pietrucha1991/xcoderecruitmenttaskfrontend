import {Component} from '@angular/core';
import {InputService} from "./service/input.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'xCodeRecrutmentTask';
  numbersSorted: number[] = [];
  myForm: FormGroup;

  constructor(private inputService: InputService) {
    this.myForm = new FormGroup({
        numbers: new FormControl('', Validators.required),
      }
    );
  }

  onSort(): number[] {
    const numbersToSort = this.myForm.controls['numbers'].value;
    console.log(numbersToSort);
    return numbersToSort;
  }

  sortNumbers(): void {
    this.inputService.sortNumbers(this.onSort()).subscribe(
      response => {
        this.numbersSorted.push(1 ,2);
        console.log(response);
      }
    )
  }


}
