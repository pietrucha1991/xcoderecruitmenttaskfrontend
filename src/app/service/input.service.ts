import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class InputService {

  private URL: string = "http:localhost:8080/numbers/sort-command"

  constructor(private http: HttpClient) {
  }

  public sortNumbers(numbers: number[]): Observable<number[]>{
      return this.http.post<number[]>(this.URL, numbers);
  }
}
